## Assignment 52 OSPF Route Redistribution
/* By Niels Rosenvold */
version 12.1X47-D15.4;
system {
    host-name R2;
    time-zone Europe/Berlin;
    /* User: root Password: Rootpass */
    root-authentication {
        encrypted-password "$1$xH9xJoL6$MFOUYnZr4.Qj2NM24XInz/";
    }
    services {
        ssh;
        dhcp-local-server { /* Opretter en gruppe for hvert interface*/
            group UserLAN1 {
                interface ge-0/0/3.0;
            }
        }
    }
}
interfaces {
    ge-0/0/1 {
        unit 0 {
            family inet {
                /* Lan 5 til R4 */
                address 10.10.10.1/28;
            }
        }
    }
    ge-0/0/2 {
        unit 0 {
            family inet {
                /* Lan10 til R5 */
                address 10.10.12.2/28;
            }
        }
    }
    ge-0/0/3 {
        unit 0 {
            family inet {
                /* UserLAN1. Selve UserLAN1 er DHCP og ikke statisk som her. Se diagram */
                address 192.168.13.1/24;
            }
        }
    }
    ge-0/0/4 {
        unit 0 {
            family inet {
                /* VMnet8 */
                address 10.56.16.80/22;
            }
        }
    }
    ge-0/0/5 {
        unit 0 {
            family inet {
                /* DMZ */
                address 192.168.10.1/24;
            }
        }
    }
    ge-0/0/6 {
        unit 0 {
            family inet {
                /* ServerLAN */
                address 192.168.11.1/24;
            }
        }

    lo0 {
        unit 0 {
            family inet {
                /* Loopback */
                address 192.168.100.1/32;
            }
        }
    }
}
routing-options {
    static {
        route 0.0.0.0/0 next-hop 10.56.16.1; /* Default Route */
    }
}
protocols {
    ospf {
        export static-route-to-internet; /* Export the default static route to the internet */
        export route-to-UserLAN1;
        area 0.0.0.0 {
            interface lo0;
            interface ge-0/0/1.0;
            interface ge-0/0/2.0;
        }
    }
}
policy-options {
    policy-statement static-route-to-internet {
        term accpt-my-default-route {
            from {
                protocol static;
                route-filter 0.0.0.0/0 exact;
            }
            then accept;
        }
    }
    policy-statement route-to-UserLAN1 {
        term accpt-route-to-UserLAN1 {
            from {
                route-filter 192.168.13.0/24 exact;
            }
            then accept;
        }
    }
}
security {
    nat {
        /* Changes the source address of egress packets */
        source {
            /* Multiple sets of rules can be set */
            rule-set trust-to-untrust {
                from zone R2_trust;
                to zone R2_untrust;
                /* Multiple rules can be set in each rule-set */
                rule rule-any-to-any {
                    match {
                        source-address 0.0.0.0/0;
                        destination-address 0.0.0.0/0;
                    }
                    then {
                        source-nat {
                            /* Use egress interface source address */
                            interface;
                        }
                    }
                }
            }
            rule-set DMZ-to-untrust {
                from zone DMZ;
                to zone R2_untrust;
                rule rule-is-any-to-any {
                    match {
                        source-address 0.0.0.0/0;
                        destination-address 0.0.0.0/0;
                    }
                    then {
                        source-nat {
                            /* Use egress interface source address */
                            interface;
                        }
                    }
                }
            }
        }  
        destination {
            pool NginxWebServer {
                address 192.168.10.50/32;
            }
            rule-set DestinationNATRuleWebServer {
                from zone R2_untrust;
                rule ruleToWebServer {
                    match {
                        destination-address 10.56.16.80/32;
                        destination-port {
                            80;
                        }
                    }
                    then {
                        destination-nat {
                            pool {
                                NginxWebServer;
                            }
                        }
                    }
                }
            }
        }
    }    
    policies {
        /* This policy is needed for interfaceses in the R2_trust zone to intercommunicate */
        from-zone R2_trust to-zone R2_trust {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone R2_untrust to-zone R2_trust { /* Politik der blokere alt trafik der kommer fra zonen 'untrust' til zonen 'trust' */
            policy default-deny {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    deny;
                }
            }
        }
        from-zone R2_trust to-zone R2_untrust { /* Politik der tillader alt trafik der kommer fra zonen 'trust' til zonen 'untrust' */
            policy internet-access {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone R2_untrust to-zone DMZ { /* Politik der tillader udefrakommende trafik til zonen DMZ. Under betingelse af at det er til den korrekte IP-adresse og af typen http. */
            policy WebServerPolicy {
                match {
                    source-address any;
                    destination-address WebServer;
                    application junos-http;
                }
                then {
                    permit;
                }
            }
        }
        from-zone DMZ to-zone R2_untrust { /* Politik der tillader alt trafik der kommer fra zonen 'DMZ' til zonen 'untrust' */
            policy outOnTheInternet {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone DMZ to-zone R2_trust { /* Politik der blokere alt trafik der kommer fra zonen 'DMZ' til zonen 'trust' */
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    deny;
                }
            }
        }
        from-zone R2_trust to-zone DMZ { /* Politik der tillader alt trafik der kommer fra zonen 'trust' til zonen 'DMZ' */
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
    }
    zones {
        security-zone R2_trust {
            interfaces {
                lo0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                        protocols {
                            ospf;
                        }
                    }
                }
                ge-0/0/1.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                        protocols {
            /* Open this interface up for participation in OSPF */
                            ospf;
                        }
                    }
                }
                ge-0/0/2.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                        protocols {
                            ospf;
                        }
                    }
                }
                ge-0/0/3.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                            dhcp;
                        }
                        protocols {
                            ospf;
                        }
                    }
                }
            }
        }
        security-zone R2_untrust {/* Untrust zonen. Ignorere trafik herfra  */
            interfaces {
                ge-0/0/4.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                    }
                }
            }
        }
        security-zone DMZ {
            address-book {
                address WebServer 192.168.10.50/32;
            }
            interfaces {
                ge-0/0/5.0 {
                    host-inbound-traffic {
                        system-services {
                            ssh;
                            dhcp;
                        }
                    }
                }
            }
        }
    }
}
access {
    /* DHCP parametre for UserLAN1. Alt mellem 1 og 254 er muligt teknisk set grundet /24 netmaske. Skal <1 pgr router er =1 */
    address-assignment {
        pool UserLAN1 {
            family inet {
                network 192.168.13.0/24;
                range USERS {
                    low 192.168.13.2;
                    high 192.168.13.15;
                }
                dhcp-attributes {
                    maximum-lease-time 600;
                    name-server {
                        8.8.8.8;
                    }
                    router {
                        192.168.13.1;
                    }
                }
            }
        }
    }
}